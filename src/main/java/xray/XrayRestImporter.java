package xray;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;

/**
 * Class for export of test execution results to
 */

public class XrayRestImporter extends XrayRestClient {
    private static Logger log = LoggerFactory.getLogger(XrayRestImporter.class);

    private String testExecutionKey;
    private String testPlanKey;
    private boolean firstConnection = true;
    private final String xrayIssueTypeTestExecution = "10007";

    public XrayRestImporter() {
        init();
    }


    public void setTestPlanKey(String testPlanKey) {
        this.testPlanKey = testPlanKey;
    }

    public void setTestExecutionKey(String testExecutionKey) {
        this.testExecutionKey = testExecutionKey;
    }

    public static void main(String args[]) throws IOException {
        XrayRestImporter importer = new XrayRestImporter();

        if ((args != null) && (args.length > 0)) {
            HashMap argMap = importer.readCommandLineArguments(args);

            log.debug("ID der Testausführung: " + (String) argMap.get("testExecutionKey"));
            importer.setTestPlanKey((String) argMap.get("testPlanKey"));
            importer.setTestExecutionKey((String) argMap.get("testExecutionKey"));
        }

        importer.sendExecutionResultsToJIRA();
        //importer.getTestsInTestplan((String) argMap.get("testExecutionKey"));

    }

    private Iterable<Path> getXMLResultFiles(String directory, String pattern) {

        Path dir = Paths.get(System.getProperty("user.dir"), directory);
        try {
            return Files.newDirectoryStream(dir, pattern);
        } catch (IOException e) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can // only be thrown by newDirectoryStream.
            log.error("can't list test result files", e);
            return Collections.emptyList();
        }
    }

    private String extractExecKey(String jsonBody) throws IOException {
        final String keyName = "key";
        HashMap<String, Object> result = new ObjectMapper().readValue(jsonBody, HashMap.class);
        HashMap bag = (HashMap) result.get("testExecIssue");
        return (String) bag.get(keyName);
    }

    /**
     * If @this.testPlanKey is defined, import into new test execution with reference to the test plan [key]
     * If @this.testExecutionKey defined, import into the referenced test execution --(with reference to @this.testPlanKey)
     * If neither of both is defined, import into new test execution
     */


    public void sendExecutionResultsToJIRA() {

        Iterable<Path> files = getXMLResultFiles("target/failsafe-reports", "TEST-*.xml");
        try {
            for (Path file : files) {
                //if (firstConnection && (testExecutionKey == null || testExecutionKey.equals(""))) {

                ObjectNode infoPayload = constructJsonInfoPayload(testPlanKey);
                log.info("INFO: " + infoPayload.toString());
                File infoFile = File.createTempFile("info-", ".json");
                Files.write(infoFile.toPath(), singletonList(infoPayload.toString()), UTF_8);

                String responseBody = sendSingleResultFileInMultipartMsg(file, infoFile.toPath());
                this.testExecutionKey = extractExecKey(responseBody);
                /*}
                else {
                    responseBody = sendSingleResultFileInMultipartMsg(Paths.get(reportDir + file.getFileName()));
                } */
                log.info(responseBody);
                infoFile.delete();
                firstConnection = false;
            }
        } catch (IOException ex) {

            log.error("Fehler: ", ex);
            //ToDo: handle exceptions from extractExecKey()
        }
    }


    private ObjectNode constructJsonInfoPayload(String testPlanKey) {
        JsonNodeFactory jnf = JsonNodeFactory.instance;
        ObjectNode payload = jnf.objectNode();
        ObjectNode fields = payload.putObject("fields");
        {
            ObjectNode project = fields.putObject("project");
            {
                project.put("id", this.projectID);
            }
            fields.put("summary", "Testlauf 30.03.00.00");
            fields.put("description", "Test Execution created while importing test results");

            ObjectNode issueType = fields.putObject("issuetype");
            {
                issueType.put("id", this.xrayIssueTypeTestExecution);
            }

            if (testPlanKey != null) {
                ArrayNode customField_10038 = fields.putArray("customField_10038");
                {
                    customField_10038.add(testPlanKey);
                }
            }
        }
        return payload; /*.toString();*/ //textValue();
    }


    private String sendSingleResultFileInMultipartMsg(Path file, Path info) {
        String encodedKey = Base64.getEncoder().encodeToString((restUser + ":" + pwd).getBytes());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic " + encodedKey);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(file));
        body.add("info", new FileSystemResource(info));
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(getUrl(testExecutionKey), HttpMethod.POST, entity, String.class);

        // ToDo: evaluate response.getStatusCode()
        final HttpStatus statusCode = response.getStatusCode();

        return response.getBody();
    }

    private String getUrl(String testExecutionKey) {

        String restUrl = xrayUri
                + "raven/1.0/import/execution/junit?projectKey=" + this.projectName; ///multipart
        restUrl += (testExecutionKey != null) ? ("&testExecKey=" + testExecutionKey) : "";

        log.info("REST Endpoint: " + restUrl);
        return restUrl;
    }
}

