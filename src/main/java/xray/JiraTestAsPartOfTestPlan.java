package xray;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JiraTestAsPartOfTestPlan {
    @JsonProperty("id")
    private String id;
    @JsonProperty("latestStatus")
    private String latestStatus;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("key")
    private String key;

}


