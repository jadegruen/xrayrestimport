package xray;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

public class XrayRestClient {
    final protected String restUser = "techuser_test";
    final protected String pwd = "techtest321";
    final protected String xrayUri = "https://blntcjira02.tollcollect.cs.tc.corp:8443/jira/rest/";
    final protected String projectID = "10403";
    final protected String projectName = "QSAVTEST";
    protected static RestTemplate restTemplate = new RestTemplate();

    //protected PropertyReader propertyReader;
    private static Logger log = LoggerFactory.getLogger(XrayRestClient.class);


    public void init() {


        String trustStorePath = System.getenv("QSAV_SYSTEMTEST_TRUSTSTORE_PATH");
        String trustStorePassword = System.getenv("QSAV_SYSTEMTEST_TRUSTSTORE_PASSWORD");

        if (trustStorePath == null || trustStorePath.isEmpty()) {
            log.error("Environment variable QSAV_SYSTEMTEST_TRUSTSTORE_PATH not set!");
            System.exit(1);
        }

        if (trustStorePassword == null || trustStorePassword.isEmpty()) {
            log.error("Environment variable QSAV_SYSTEMTEST_TRUSTSTORE_PASSWORD not set!");
            System.exit(1);
        }

        System.setProperty("javax.net.ssl.trustStore", trustStorePath);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
    }

    public List<JiraTestAsPartOfTestPlan> getTestsInTestplan(String testPlanKey) {
        String endpoint = xrayUri
                + "/raven/1.0/api/testplan/"
                + testPlanKey
                + "/test";
        final ResponseEntity<String> response = restRequest(endpoint);

        ObjectMapper mapper = new ObjectMapper();
        String jsonBody = response.getBody();
        List<JiraTestAsPartOfTestPlan> testPlanArray = new ArrayList<>();
        try {
            testPlanArray = Arrays.asList(mapper.readValue(jsonBody, JiraTestAsPartOfTestPlan[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return testPlanArray;
    }

    public List<JiraTestExec> getTestsInTestExecution(String testExecKey) {
        String endpoint = xrayUri
                + "/raven/1.0/api/testexec/"
                + testExecKey
                + "/test";

        final ResponseEntity<String> response = restRequest(endpoint);

        ObjectMapper mapper = new ObjectMapper();
        String jsonBody = response.getBody();
        log.info("Received data out of test execution: " + response.getBody());

        List<JiraTestExec> testPlanArray = new ArrayList<>();
        try {
            testPlanArray = Arrays.asList(mapper.readValue(jsonBody, JiraTestExec[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return testPlanArray;
    }

    /**
     * The method sends a REST request to the endpoint
     */

    protected ResponseEntity<String> restRequest(String endpoint) {
        log.info("Retrieving information from REST endpoint " + endpoint + "...");

        String encodedKey = Base64.getEncoder().encodeToString((restUser + ":" + pwd).getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.set("Authorization", "Basic " + encodedKey);

        HttpEntity<String> getEntity = new HttpEntity<>("", headers);

        final ResponseEntity<String> response = restTemplate.exchange(endpoint,
                HttpMethod.GET,
                getEntity,
                String.class);

        log.debug("Code received from Xray/JIRA server:" + response.getStatusCode());


        return response;
    }

    protected HashMap readCommandLineArguments(String[] args) {

        if ( (args[0] == null) || (args[0].isEmpty())) return null;
        HashMap map = new HashMap();
        //String[] arguments = args.split(" ");

        for (String arg : args) {
            String[] keysValues = arg.split("=");
            if (keysValues[0].startsWith("-"))
                keysValues[0] = keysValues[0].substring(1);
            map.put(keysValues[0], keysValues[1]);
        }
        return map;
    }
}
