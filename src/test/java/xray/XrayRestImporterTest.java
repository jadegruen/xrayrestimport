package xray;

import org.junit.Test;

class XrayRestImporterTest {

    @Test
    public void importWithKnownTestPlan() {
        XrayRestImporter importer = new XrayRestImporter();
        importer.setTestPlanKey("QSAVTEST-3");
        //ToDo: file with filesafe output
        importer.sendExecutionResultsToJIRA();

    }

    @Test
    public void importWithKnownTestExecution() {
        XrayRestImporter importer = new XrayRestImporter();
        importer.setTestExecutionKey("QSAVTEST-1196");
        //ToDo: file with filesafe output
        importer.sendExecutionResultsToJIRA();
    }

    @Test
    public void importWithoutTPOrTE() {
        XrayRestImporter importer = new XrayRestImporter();
        //ToDo: file with filesafe output
        importer.sendExecutionResultsToJIRA();
    }


}